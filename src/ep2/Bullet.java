package ep2;

import java.util.Random;

public class Bullet extends Sprite {
    private static final int MAX_SPEED_Y = 3;
    private int speed_y;
    private int speed_x;

    public Bullet(int x, int y){
	super(x, y);		
	initBullet();
	speed_y = MAX_SPEED_Y;

	Random gerador = new Random();

	speed_x = gerador.nextInt(5) - 2;

    }

    public void initBullet(){
	loadImage("images/bullet.png");
    }

    public void move(){		
	y += speed_y;
	x += speed_x;
	if (y > (Game.getHeight() + height)){
            visible = false;
	}
    }
}

