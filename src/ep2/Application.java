package ep2;

import java.awt.EventQueue;
import javax.swing.JFrame;


public class Application extends JFrame {
    private Menu menu;
    private Map game;
    
    public Application() {
        menu = new Menu(this);
        game = new Map(this);
        
        add(menu);

        setSize(Game.getWidth(), Game.getHeight());

        setTitle("Space Combat Game");
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
    }
    
    public void showMenu(){
        game.pause();
        remove(game);

        add(menu);
        revalidate();            
        menu.requestFocusInWindow();        
        menu.repaint();
    }
    
    public void startGame(){
        remove(menu);        
        add(game);
        game.reset();
        game.start();
        revalidate();            
        game.requestFocusInWindow();
    }
    
    
    public static void main(String[] args) {
        
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                Application app = new Application();
                app.setVisible(true);
            }
        });
    }

    
}