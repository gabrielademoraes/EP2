package ep2;

import java.util.ArrayList;

public class Alien extends Sprite {
    private static final int MAX_SPEED_Y = 1;
    private int speed_y;
    private int c = 1;    
    private int points;
    private int nbullet;
    private Map map;

    public Alien(int x, int y, Map map, int level){
	super(x, y);				
	speed_y = MAX_SPEED_Y;
        this.map = map;	
	switch (level){			
            case 0:
		loadImage("images/alien_EASY.png");
		points = 10;
		nbullet = 100;
            break;		
            case 1:
		loadImage("images/alien_MEDIUM.png");
		points = 20;
		nbullet = 70;
            break;
            case 2:
		loadImage("images/alien_HARD.png");
		points = 30;
		nbullet = 20;
            break;
	}
    }

    public void move(){
	y += speed_y;
	if (y > Game.getHeight()){
            visible = false;
	}

	if ((c % nbullet) == 0){ 
            Bullet bullet = new Bullet(x, y);
            bullet.setX(x + (width - bullet.getWidth())/2);
            bullet.setY(y + bullet.getHeight());            
            this.map.addSprite(bullet);			
	}

	c++;
    }
}