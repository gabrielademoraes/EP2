package ep2;

import java.util.ArrayList;

public class Bonus extends Sprite {
    private static final int MAX_SPEED_Y = 2;
    private int speed_y;    
    private int addLife;
	
	
    public Bonus(int x, int y){
        super(x,y); 
        speed_y = MAX_SPEED_Y;        
        loadImage("images/laserGreenShot.png");
        addLife = 1;
    }

    public int getAddLife(){
        return addLife;
    }
   
    public void move(){		
        y += speed_y;
        
        if (y > Game.getHeight()){
            visible = false;
        }
    }
    
    
}

