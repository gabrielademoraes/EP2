package ep2;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

public class Menu extends JPanel implements ActionListener {        
    private final Image background;
    private Image ajuda;
    private Image creditos;
    private Image menuPrincipal;
    private Font fontInfo;
    private Application app;
    private KeyListerner keyListerner;
    private Image[] btnOn;
    private Image[] btnOff;
    private int nav = 0;
    private int sel = 0;
    private Menu menu;

    public Menu(Application app) {

        this.app = app;
        ImageIcon image;

        keyListerner = new KeyListerner();
        addKeyListener(keyListerner);
        
        setFocusable(true);
        setDoubleBuffered(true);

        btnOn = new Image[4];
        image = new ImageIcon("images/iniciar_on.png");        
        btnOn[0] = image.getImage();        
        image = new ImageIcon("images/ajuda_on.png");        
        btnOn[1] = image.getImage();
        image = new ImageIcon("images/creditos_on.png");        
        btnOn[2] = image.getImage();
        image = new ImageIcon("images/sair_on.png");        
        btnOn[3] = image.getImage();

        btnOff = new Image[4];
        image = new ImageIcon("images/iniciar_off.png");        
        btnOff[0] = image.getImage();        
        image = new ImageIcon("images/ajuda_off.png");        
        btnOff[1] = image.getImage();
        image = new ImageIcon("images/creditos_off.png");        
        btnOff[2] = image.getImage();
        image = new ImageIcon("images/sair_off.png");        
        btnOff[3] = image.getImage();
        
        image = new ImageIcon("images/menu.png");        
        this.background = image.getImage();

        image = new ImageIcon("images/ajuda.png");      
        this.ajuda = image.getImage();

        image = new ImageIcon("images/creditos.png");      
        this.creditos = image.getImage();

        image = new ImageIcon("images/menu_principal.png");      
        this.menuPrincipal = image.getImage();
       
        


        Font fontInfo = new Font("Helvetica", Font.BOLD, 14);                

    }
    
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        switch(sel){
            case 0:
            g.drawImage(this.background, 0, 0, null);
            for (int i = 0; i < 4; i++){
                if (nav == i){
                    g.drawImage(btnOn[i], (Game.getWidth()/2)-75, 100+(i*60), null);            
                }
                else{
                    g.drawImage(btnOff[i], (Game.getWidth()/2)-75, 100+(i*60), null);                            
                }
            }
            break;
            case 1: // Ajuda
                g.drawImage(this.ajuda, 0, 0, null);
                g.drawImage(menuPrincipal, (Game.getWidth()/2)-75, Game.getHeight()-120, null);                            
            break;
            case 2: // Créditos
                g.drawImage(this.creditos, 0, 0, null);
                g.drawImage(menuPrincipal, (Game.getWidth()/2)-75, Game.getHeight()-120, null);                            
            break;

        }  
        

        Toolkit.getDefaultToolkit().sync();
    }

    @Override
    public void actionPerformed(ActionEvent e) {        
    }
    

    public void pressed(KeyEvent e) {

        int key = e.getKeyCode();  

        
        if (sel == 0 ){
            if (key == KeyEvent.VK_ENTER) {
                if (nav == 0){                
                    app.startGame();                                
                }
                else if(nav == 1){                
                    sel = 1;                
                }            
                else if(nav == 2){                                
                    sel = 2;
                }             
                else if (nav == 3){
                    System.exit(0);
                }            
            }            
            if (key == KeyEvent.VK_UP) {
                nav--;
                if (nav < 0){
                    nav = 0;
                }
            }        

            if (key == KeyEvent.VK_DOWN) {
                nav++;
                if (nav > 3){
                    nav = 3;
                }
            }
        }
        else{
            if (key == KeyEvent.VK_ENTER) {
                sel = 0;
            }
        }

        repaint();      
        
    }
    
    
    private class KeyListerner extends KeyAdapter {

        @Override
        public void keyPressed(KeyEvent e) {            
            pressed(e);
        }

        @Override
        public void keyReleased(KeyEvent e) {

        }
        
    }

}