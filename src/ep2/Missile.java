package ep2;

public class Missile extends Sprite {
	private static final int MAX_SPEED_Y = 3;
	private int speed_y;

	public Missile(int x, int y){
            super(x, y);		
            initMissile();
            speed_y = -1 * MAX_SPEED_Y;
	}

	public void initMissile(){
            loadImage("images/missile.png");
	}

	public void move(){		
            y += speed_y;
            if (y < (height * -1)){
		visible = false;
            }
	}
}