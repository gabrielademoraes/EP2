package ep2;

import java.util.Random;

public class Explosion extends Sprite {	
	private int c;

	public Explosion(int x, int y){
            super(x, y);		
            initExplosion();		
            c = 10;
	}

	public void initExplosion(){
            loadImage("images/explosion.png");
	}

	public void move(){				
            c--;
            if (c < 0){
                visible = false;
            }
	}
}
