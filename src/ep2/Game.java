package ep2;

public final class Game {
    
    private static final int WIDTH = 500;
    private static final int HEIGHT = 500;
    private static final int DELAY = 10;
    
    private static final int MAX_EASY = 200;
    private static final int MAX_MEDIUM = 400;
    private static final int MAX_HARD = 500;
    private static int level;
    
    public static int getWidth(){
        return WIDTH;
    }
    
    public static int getHeight(){
        return HEIGHT;
    }
    
    public static int getDelay(){
        return DELAY;
    }
    
     public static int getLevel(){
        return level;
    }

    public static int getMaxEasy() {
        return MAX_EASY;
    }

    public static int getMaxMedium() {
        return MAX_MEDIUM;
    }

    public static int getMaxHard() {
        return MAX_HARD;
    }
}


