/*
package ep2;

import java.awt.event.KeyEvent;
import java.util.ArrayList;

public class Spaceship extends Sprite {
    
    private static final int MAX_SPEED_X = 2;
    private static final int MAX_SPEED_Y = 1;
   
    private int speed_x;
    private int speed_y;
    private Map map;

    public Spaceship(int x, int y, Map map) {
        super(x, y);

        this.map = map;        
        initSpaceShip();
    }

    private void initSpaceShip() {        
        noThrust();        
    }

    public void stop(){
        speed_x = 0;
        speed_y = 0;
    }
    
    private void noThrust(){
        loadImage("images/spaceship.png"); 
    }
    
    private void thrust(){
        loadImage("images/spaceship_thrust.png"); 
    }    

    public void move() {
        
        // Limits the movement of the spaceship to the side edges.
        if((speed_x < 0 && x <= 0) || (speed_x > 0 && x + width >= Game.getWidth())){
            speed_x = 0;
        }
        
        // Moves the spaceship on the horizontal axis
        x += speed_x;
        
        // Limits the movement of the spaceship to the vertical edges.
        if((speed_y < 0 && y <= 0) || (speed_y > 0 && y + height >= Game.getHeight())){
            speed_y = 0;
        }

        // Moves the spaceship on the verical axis
        y += speed_y;
        
    }

    public void keyPressed(KeyEvent e) {

        int key = e.getKeyCode();
        
        // Set speed to move to the left
        if (key == KeyEvent.VK_LEFT) { 
            speed_x = -1 * MAX_SPEED_X;
        }

        // Set speed to move to the right
        if (key == KeyEvent.VK_RIGHT) {
            speed_x = MAX_SPEED_X;
        }
        
        // Set speed to move to up and set thrust effect
        if (key == KeyEvent.VK_UP) {
            speed_y = -1 * MAX_SPEED_Y;
            thrust();
        }
        
        // Set speed to move to down
        if (key == KeyEvent.VK_DOWN) {
            speed_y = MAX_SPEED_Y;
        }

        if (key == KeyEvent.VK_SPACE){
            Missile missile = new Missile(x, y);
            missile.setX(x + (width - missile.getWidth())/2);
            missile.setY(y - missile.getHeight());
            this.map.addSprite(missile);         
        }

        if (key == KeyEvent.VK_P){
            if (this.map.isPause()){
                this.map.start();            
            }
            else{
                this.map.pause();            
            }
        }

        if (key == KeyEvent.VK_ESCAPE){
            this.map.exit();
        }

        
    }
    
    public void keyReleased(KeyEvent e) {

        int key = e.getKeyCode();

        if (key == KeyEvent.VK_LEFT || key == KeyEvent.VK_RIGHT) {
            speed_x = 0;
        }

        if (key == KeyEvent.VK_UP || key == KeyEvent.VK_DOWN) {
            speed_y = 0;
            noThrust();
        }
    }
}
*/
package ep2;

import java.awt.event.KeyEvent;
import java.util.ArrayList;

public class Spaceship extends Sprite {
    
    private static final int MAX_SPEED_X = 2;
    private static final int MAX_SPEED_Y = 1;
   
    private int speed_x;
    private int speed_y;
    private Map map;
    private int life = 10;
    private int points = 0;

    public Spaceship(int x, int y, Map map) {
        super(x, y);

        this.map = map;        
        initSpaceShip();
    }

    private void initSpaceShip() {        
        noThrust();        
    }


    public void stop(){
        speed_x = 0;
        speed_y = 0;
    }

    public void reset(){
        this.life = 10;
        this.points = 0;
    }

    public void decLife(){
        this.life--;
        if (this.life<=0){
            this.map.setGameOver();            
        }
    }

    public void addLife(){
        this.life++;
    }

    public void setLife(int life){
        this.life = life;
    }

    public void addPoints(int points){
        this.points += points;
    }

    public int getPoints(){
        return points;
    }

    public int getLife(){
        return life;
    }
    
    private void noThrust(){
        loadImage("images/spaceship.png"); 
    }
    
    private void thrust(){
        loadImage("images/spaceship_thrust.png"); 
    }    

    public void move() {
        
        // Limits the movement of the spaceship to the side edges.
        if((speed_x < 0 && x <= 0) || (speed_x > 0 && x + width >= Game.getWidth())){
            speed_x = 0;
        }
        
        // Moves the spaceship on the horizontal axis
        x += speed_x;
        
        // Limits the movement of the spaceship to the vertical edges.
        if((speed_y < 0 && y <= 0) || (speed_y > 0 && y + height >= Game.getHeight())){
            speed_y = 0;
        }

        // Moves the spaceship on the verical axis
        y += speed_y;
        
    }

    public void keyPressed(KeyEvent e) {

        int key = e.getKeyCode();
        
        // Set speed to move to the left
        if (key == KeyEvent.VK_LEFT) { 
            speed_x = -1 * MAX_SPEED_X;
        }

        // Set speed to move to the right
        if (key == KeyEvent.VK_RIGHT) {
            speed_x = MAX_SPEED_X;
        }
        
        // Set speed to move to up and set thrust effect
        if (key == KeyEvent.VK_UP) {
            speed_y = -1 * MAX_SPEED_Y;
            thrust();
        }
        
        // Set speed to move to down
        if (key == KeyEvent.VK_DOWN) {
            speed_y = MAX_SPEED_Y;
        }

        if (key == KeyEvent.VK_SPACE){
            Missile missile = new Missile(x, y);
            missile.setX(x + (width - missile.getWidth())/2);
            missile.setY(y - missile.getHeight());
            this.map.addSprite(missile);         
        }        
    }
    
    public void keyReleased(KeyEvent e) {

        int key = e.getKeyCode();

        if (key == KeyEvent.VK_LEFT || key == KeyEvent.VK_RIGHT) {
            speed_x = 0;
        }

        if (key == KeyEvent.VK_UP || key == KeyEvent.VK_DOWN) {
            speed_y = 0;
            noThrust();
        }
    }
}