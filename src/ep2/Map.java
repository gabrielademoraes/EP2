package ep2;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;
import java.util.ArrayList;
import java.util.Random;
import java.util.Iterator;
import java.io.File;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public class Map extends JPanel implements ActionListener {

    private final int SPACESHIP_X = 220;
    private final int SPACESHIP_Y = 430;
    private final Timer timer_map;        
    private int level = 1;

    
    private final Image background;
    private final Spaceship spaceship;
    private Application app;
    private ArrayList<Sprite> sprites;
    private int c;
    private Random gerador;
    private Font fontInfo;
    private boolean pause;    
    private boolean gameOver;
    private boolean victory;
    

    public Map(Application app) {
        this.app = app;
        
        addKeyListener(new KeyListerner());
        
        setFocusable(true);
        setDoubleBuffered(true);

        gerador = new Random();

        ImageIcon image = new ImageIcon("images/space.jpg");
        
        this.background = image.getImage();
        
        sprites = new ArrayList<Sprite>();       
        spaceship = new Spaceship(SPACESHIP_X, SPACESHIP_Y, this);
        sprites.add(spaceship);

        timer_map = new Timer(Game.getDelay(), this);
        

        Font fontInfo = new Font("Helvetica", Font.BOLD, 16);        
        c = 0;    

        this.pause = true;        
    }
    
    public void playSound(String file) {
        try {
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File(file).getAbsoluteFile());
            Clip clip = AudioSystem.getClip();
            clip.open(audioInputStream);
            clip.start();
        } catch(Exception ex) {
            System.out.println("Nao consegui tocar o audio");            
        }
    }

    public void setGameOver(){
        this.gameOver = true;
        this.pause();
    }

    public void reset(){        
        this.level = 1;
        this.gameOver = false;

        spaceship.setX(SPACESHIP_X);        
        spaceship.setY(SPACESHIP_Y);
        spaceship.stop();
        spaceship.reset();

        sprites.clear();
        sprites.add(spaceship);
    }

    public void start(){                
        timer_map.start();            
        pause = false;
    }

    public void pause(){
        timer_map.stop();
        pause = true;
    }

    public void exit(){        
        app.showMenu();
    }

    public boolean isPause(){
        return (pause == true);
    }    

    public void addSprite(Sprite sprite){
        this.sprites.add(sprite);
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        g.drawImage(this.background, 0, 0, null);

        draw(g);

        Toolkit.getDefaultToolkit().sync();
    }
    
    private void draw(Graphics g) {        
        // Draw spaceship
       
        for (Sprite sprite : sprites){            
            g.drawImage(sprite.getImage(), sprite.getX(), sprite.getY(), this);     
            
        }

        String message = "Vidas: "+spaceship.getLife()+" / Pontos: "+spaceship.getPoints();        
        g.setColor(Color.white);
        g.setFont(fontInfo);
        g.drawString(message, 10, 20);

        if(spaceship.getPoints() == Game.getMaxEasy()){
            drawLevelEasy(g);
        }
        else if (spaceship.getPoints() == Game.getMaxMedium()){
            drawLevelMedium(g);
        }
        else if (spaceship.getPoints() == Game.getMaxHard()){
            app.showMenu();
        }
        if (gameOver){
            drawGameOver(g);
        }
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        ArrayList<Sprite> removeSprites = new ArrayList<Sprite>();        
        ArrayList<Sprite> navSprites = new ArrayList<Sprite>();                
        
        switch(level){
            case 1:
            if((c % 1000) == 100){
                Bonus bonus = new Bonus(gerador.nextInt(Game.getWidth()), 0);
                bonus.setY(-1 * bonus.getHeight());
                sprites.add(bonus);
                System.out.println("Add Bonus"); 

            }
            if ((c % 200) == 0){
                Alien alien = new Alien(gerador.nextInt(Game.getWidth()), 0, this, gerador.nextInt(3));
                alien.setY(-1 *  alien.getHeight());
                sprites.add(alien);
                System.out.println("Add Alien"); 


                if(spaceship.getPoints() == Game.getMaxEasy()){
                    spaceship.setLife(10);
                    level = 2;
                }
            }
            break;
            
            case 2:
            if((c % 5000) == 100){
                Bonus bonus = new Bonus(gerador.nextInt(Game.getWidth()), 0);
                bonus.setY(-1 * bonus.getHeight());
                sprites.add(bonus);
                System.out.println("Add Bonus"); 

            }

            if ((c % 100) == 0){
                Alien alien = new Alien(gerador.nextInt(Game.getWidth()), -100, this, gerador.nextInt(3));
                sprites.add(alien);
                System.out.println("Add Alien");

                if(spaceship.getPoints() == Game.getMaxMedium()){    
                    spaceship.setLife(10);
                    level = 3;                    
                }
            }

            break;
            
            case 3:
            if((c % 8000) == 100){
                Bonus bonus = new Bonus(gerador.nextInt(Game.getWidth()), 0);
                bonus.setY(-1 * bonus.getHeight());
                sprites.add(bonus);
                System.out.println("Add Bonus"); 

            }

            if ((c % 80) == 0){
                Alien alien = new Alien(gerador.nextInt(Game.getWidth()), -100, this, gerador.nextInt(3));
                sprites.add(alien);
                System.out.println("Add Alien");            
            }

            break;

        }
        
        navSprites.addAll(sprites);        
        Explosion explosion;
        Sprite a, b, s;
        Bonus bonus;

        int size = navSprites.size();
        for (int i = 0; i < size; i++){            
            a = navSprites.get(i); 
            a.move();
            if (!a.isVisible()){
                removeSprites.add(a);
                continue;
            }

            for (int j= (i+1); j < size; j++){
                b = navSprites.get(j);
                if((a instanceof Spaceship) && (b instanceof Bonus)){
                    if(a.isCollision(b.getBounds())){
                        bonus = (Bonus)b;                        
                        removeSprites.add(b);                        
                        playSound("sounds/bonus.wav");
                        spaceship.addLife();                    }

                    }
                    if (((a instanceof Missile) && (b instanceof Alien)) || ((a instanceof Alien) && (b instanceof Missile))){
                        if (a.isCollision(b.getBounds())){                    
                            removeSprites.add(a);
                            removeSprites.add(b);                                                
                            if (a instanceof Alien){                            
                                s = a;
                            }
                            else{
                                s = b;
                            }
                            explosion = new Explosion(0, 0);
                            explosion.setX(s.getX() + (s.getWidth() - explosion.getWidth())/2);
                            explosion.setY(s.getY() + (s.getHeight() - explosion.getHeight())/2);                                                    
                            sprites.add(explosion);

                            playSound("sounds/explosion.wav");

                            spaceship.addPoints(10);                        

                            break;                    
                        }
                    }
                    if (((a instanceof Spaceship) && (b instanceof Bullet)) || ((a instanceof Spaceship) && (b instanceof Alien))){
                        if (a.isCollision(b.getBounds())){                    
                            if (a instanceof Bullet){
                                removeSprites.add(a);
                                s = b;
                            }
                            else{
                                removeSprites.add(b);
                                s = a;
                            }

                            explosion = new Explosion(0, 0);
                            explosion.setX(s.getX() + (s.getWidth() - explosion.getWidth())/2);
                            explosion.setY(s.getY() + (s.getHeight() - explosion.getHeight())/2);                                                    
                            sprites.add(explosion);

                            playSound("sounds/spaceship.wav");

                            spaceship.decLife();
                            break;                    
                        }
                    }
                }

            }

            System.out.println("S = "+sprites.size());

            sprites.removeAll(removeSprites);             

            repaint();    
            c++;
        }



        private void drawMissionAccomplished(Graphics g) {

            String message = "MISSION ACCOMPLISHED";
            Font font = new Font("Helvetica", Font.BOLD, 14);
            FontMetrics metric = getFontMetrics(font);

            g.setColor(Color.white);
            g.setFont(font);
            g.drawString(message, (Game.getWidth() - metric.stringWidth(message)) / 2, Game.getHeight() / 2);
        }

        private void drawGameOver(Graphics g) {

            String message = "Game Over";
            Font font = new Font("Helvetica", Font.BOLD, 14);
            FontMetrics metric = getFontMetrics(font);

            g.setColor(Color.white);
            g.setFont(font);
            g.drawString(message, (Game.getWidth() - metric.stringWidth(message)) / 2, Game.getHeight() / 2);
        }

        private void drawLevelEasy(Graphics g) {

            String message = "Level Easy Complete";
            Font font = new Font("Helvetica", Font.BOLD, 14);
            FontMetrics metric = getFontMetrics(font);

            g.setColor(Color.white);
            g.setFont(font);
            g.drawString(message, (Game.getWidth() - metric.stringWidth(message)) / 2, Game.getHeight() / 2);
        }

        private void drawLevelMedium(Graphics g) {

            String message = "Level Medium Complete";
            Font font = new Font("Helvetica", Font.BOLD, 14);
            FontMetrics metric = getFontMetrics(font);

            g.setColor(Color.white);
            g.setFont(font);
            g.drawString(message, (Game.getWidth() - metric.stringWidth(message)) / 2, Game.getHeight() / 2);
        }
        
        private class KeyListerner extends KeyAdapter {

            @Override
            public void keyPressed(KeyEvent e) {
                int key = e.getKeyCode();

                spaceship.keyPressed(e);

                if ((key == KeyEvent.VK_ENTER)&&(gameOver)){
                    app.showMenu();                
                }
                else if ((key == KeyEvent.VK_P)&&(!gameOver)){
                    if (isPause()){
                        start();            
                    }
                    else{
                        pause();            
                    }
                }
                else if (key == KeyEvent.VK_ESCAPE){
                    exit();
                }        
            }

            @Override
            public void keyReleased(KeyEvent e) {
                spaceship.keyReleased(e);
            }

        }
    }